class Human {
  /*
   * name: String
   * birthDate: String 30-07-1999
   * address: String
   * */
  constructor(name, birthDate, address) {
    this.name = name;
    this.address = address;
    this.birthDate = birthDate;

    // Attribute yang ga langsung dari parameter 
    // constructor.
    this.birthYear = this.getYearFromBirthDate();
  }

  // Metode untuk mendapatkan tahun dari suatu birth date
  // Dipisah, biar lebih gampang dibaca, dan tidak membebani
  // constructor, biar ga banyak line-nya juga.
  getYearFromBirthDate() {
    const birthDateAsArray = this.birthDate.split('-')
    return birthDateAsArray[2]
  }

  // Psuedo Attribute
  get age() {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    return currentYear - this.birthYear;
  }

  get birthDate() {
    return this._birthDate;
  }

  set birthDate(value) {
    this._birthDate = value;
    this.birthYear = this.getYearFromBirthDate(); 
  }

  introduce() {
    console.log(`Halo, my name is ${this.name}`);
  }
}

module.exports = Human;
