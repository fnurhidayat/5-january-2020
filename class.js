// Ada dua variable atau method di dalam suatu kelas.
// 1. Static
// 2. Instance/Prototype
class Person {

  // Static Property, Variable, atau Attribute
  static isVertebrate = true;
  static contohMethod() {
    return 'Ini contoh value';
  }

  constructor(name, birthDate, address) {
    this.name = name; // Attibute, Property, bisa disebut Variable dalam kelas. Kalo di Javascript
    this.birthDate = birthDate;
    this.address = address;
  }

  // Cara mendefinisikan method di dalam suatu kelas.
  // Tanpa menggunakan keyword function.
  introduce() {
    console.log(`Halo nama saya ${this.name}, saya di tinggal di ${this.address}`)
  }

  // Cara mendefinisikan method di dalam suatu kelas.
  // Tanpa menggunakan keyword function, tapi menggunakan arrow function.
  kenalan = (greetingWords) => {
    console.log(`${greetingWords} nama saya ${this.name}, saya di tinggal di ${this.address}`)
  }
}

// function addTwo(x) {
//   return x + 2;
// }
// 
// addTwo(2) // 4
//
// console.log({
//   name: 'Fikri',
//   birthDate: '30-07-1999',
//   address: 'Solo'
// })
// 
// console.log({
//   name: 'Ihsan',
//   birthDate: '30-07-1999',
//   address: 'Solo'
// })
//
// extends inheritance

// Instantiate/Create new Object from class
const fikri = new Person('Fikri', '30-07-1999', 'Solo') // Instance Object, Prototype, Member
fikri.kenalan('Hei')

// Instance
const ihsan = new Person('Ihsan', '30-07-1999', 'Solo') // Instance Object, Prototype, Member
ihsan.introduce()

console.log(Person.isVertebrate) // Panggil dari kelas langsung
console.log(ihsan.constructor.isVertebrate) // Panggil dari instance

console.log(Person.contohMethod());
console.log(fikri.constructor.contohMethod());

// Bikin kelas bernama Human
// Yang punya property Nama, Tahun Lahir, Alamat (Pake bahasa Inggris).
// .getAge() => Mencari tahun sekarang (silahkan googling) dikurangi tahun lahir.
// Get current year Javascript.
