/*
 * Encapsulation
 *
 * Menyembunyikan suatu attribute atau method.
 * Si password, saldo.
 *
 * Accessibility (Access Modifier)
 * - Public
 * - Protected
 * - Private
 * */

class BankAccount {
  // Property ini cuma bisa diakses di dalam
  // class declaration aja.
  // Diluar itu pasti error.
  #balance = 0;

  constructor(holder, number) {
    this.holder = holder;
    this.number = number;
    this._iniProtectedAttribute = 10;
  }

  get balance() {
    return this.#balance
  }

  // Ini juga sebuah contoh abstraksi.
  // Kita ga perlu tau si method ini ngapain aja
  // Yang perlu kita tau, dia memberikan balance dari
  // Bank BCA.
  getBalanceFromBCA() {
    const response = this.#manggilAPIBCA()
    const data = JSON.parse(response.body)
    return data.balance 
  }

  // Kesepakatan dari Javascript developer
  // Kalo nama method atau attribute diawali dengan underscore
  // Maka itu artinya adalah protected method.
  // Dimana method/attribute itu hanya boleh diakses di dalam kelas itu sendiri
  // atau di child class-nya.
  _findNIK() {
    console.log('Melakukan request ke API BCA')
    return JSON.stringify({
      nik: '1234123412341234'
    })
  }

  #privateMethod() {
    return 'Ini private method'
  }

  #manggilAPIBCA() {
    console.log('Melakukan request ke API BCA')
    return JSON.stringify({
      balance: 8000
    })
  }
}

class Gopay extends BankAccount {
  getNIKFromBankAccount() {
    return this._findNIK()
  }
}


const fikri = new BankAccount('Fikri', '1234123412341234')
const fikriGo = new Gopay('Fikri', '1234123412341234')

// Access Public
console.log(fikri.balance)
console.log(fikriGo.getNIKFromBankAccount())
console.log(fikriGo._findNIK());
