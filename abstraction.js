// Abstraction, mengeneralisir sesuatu menjadi sesuatu yang simpel, atau penjelasan pada umumnya adalah:
// Menyederhanakan sesuatu dan menyembunyikan detail implementasi-nya untuk dipikirkan nanti.
// Programmer, Chef, Doctor. => Sama-sama Manusia.

// Abstract Class. => Ga boleh ada instance-nya.
// Sebuah kelas yang dimaksudkan untuk diinherit, untuk implementasinya.
class Human {
  constructor(name, birthDate, address) {

    // Untuk nge-prevent seseorang
    // melakukan instantiate di class Human,
    // secara langsung
    if (this.constructor.name === 'Human') {
      throw new Error('Cannot instantiate from abstract class')
    }

    this.name = name;
    this.birthDate = birthDate;
    this.address = address;
  }

  introduce() {
    console.log(`Hi, my name is ${this.name} and I'm a ${this.constructor.name}`)
  }
}

// Polymorphism dari kelas Human.
class Programmer extends Human {
  constructor(name, birthDate, address, programmingLanguage) {
    super(name, birthDate, address)
    this.programmingLanguage = programmingLanguage
  }

  code() {
    console.log(`Programming ${this.programmingLanguage}`)
  }
}
class Doctor extends Human {
  constructor(name, birthDate, address, specialist) {
    super(name, birthDate, address)
    this.specialist = specialist
  }

  surgery() {
    console.log(`Opening ${this.specialist} clinic`)
  }
}
class Chef extends Human {
  constructor(name, birthDate, address, cuisine) {
    super(name, birthDate, address)
    this.cuisine = cuisine
  }

  cook() {
    console.log(`Cooking some ${this.cuisine}`)
  }
}

// Coba uncomment code berikut, ini pasti error.
// new Human() => Ini ga boleh, karena kalo kayak gini, berarti bukan abstract class. 

// Tiap sub class dari Human, ini bisa introduce, disertai nama class.
// Contoh: Hi, I'm nama, and I'm a Programmer.
//
// Programmer:
// - programmingLanguage String
// Chef:
// - cuisine String 
// Doctor:
// - specialist String 
// Programmer.protoype.code() => console.log apapun, yang penting contain Programmer.prototype.programmingLanguage

const programmer = new Programmer('Fikri', '30-07-1999', 'Solo', 'Javascript')
const doctor = new Doctor('Fikri', '30-07-1999', 'Solo', 'Dentist')
const chef = new Chef('Fikri', '30-07-1999', 'Solo', 'Pizza')

console.log(programmer)
console.log(doctor)
console.log(chef)

programmer.introduce()
doctor.introduce()
chef.introduce()

programmer.code()
doctor.surgery()
chef.cook()
