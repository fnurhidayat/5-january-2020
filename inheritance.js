/*
 * Inheritance/Pewarisan.
 * Ada yang namanya Parent Class/Super Class,
 * dimana dia adalah suatu class memiliki definisi, contoh: Human,
 * yang nanti dipakai untuk Child Class/Sub Class.
 *
 * Human, => Super Class / Parent Class
 *
 * Sub class / Child Class
 * - Programmer 
 * - Chef
 * - Police
 *
 * Interface, dan Mix-Ins. => Mewarisi sesuatu secara partial.
 * */
const Human = require('./human')

/* extends
 * Coba bikin suatu kelas bernama Programmer.
 * Dimana dia adalah child class dari Human,
 * yang memiliki property:
 * - programmingLanguages Array<String> contoh: Javascript, Python, Ruby dll.
 * - platforms Array<String> contoh: Backend, Fullstack, React Native
 *
 * How to extends a class in Javascript.
 * */

class Programmer extends Human {
  // Super, ini memanggil method bernama constructor di super class.
  // Super class disini Human.
  // Method Overload
  constructor(name, birthDate, address, programmingLanguages, platforms) {
    super(name, birthDate, address)
    this.programmingLanguages = programmingLanguages
    this.platforms = platforms
  }

  // Super itu this,
  // tapi this untuk Super Class.
  // Method Override
  introduce() {
    super.introduce() // Memanggil Human.prototype.introduce()
    console.log(`And I can code ${this.programmingLanguages}`)
  }
}

const fikri = new Programmer(
  'Fikri',
  '30-07-1999',
  'Solo',
  ['Javascript', 'Python'],
  ['Backend', 'React Native']
);

console.log('Programming Languages:', fikri.programmingLanguages)
console.log('Age:', fikri.age)

// Ketika kita rubah ini, kita bisa melakukan sesuatu, contoh: Merubah Birth Year sekaligus.
fikri.birthDate = '30-07-1998'; 
fikri.birthDate = '30-07-2000';
console.log('Age:', fikri.age);
